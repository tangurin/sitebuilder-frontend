import Vue from 'vue';

/**
* State
*/

export const state = () => ({
});

/**
* Getters
*/

export const getters = {
};

/**
* Mutations
*/

export const mutations = {
};

/**
* Actions
*/

export const actions = {
    async nuxtServerInit({ dispatch }) {
        const blocks = await dispatch('blockz/refresh', {}, { root: true });
    }
};
