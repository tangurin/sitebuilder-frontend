import Vue from 'vue';
import { mergeDeep } from '@/assets/js/helpers.js';

/**
* State
*/

export const state = () => ({
    blocks: {
        root: {
            type: 'root',
            children: [],
        },
    },
});

/**
* Getters
*/

export const getters = {
};

/**
* Mutations
*/

export const mutations = {
    updateBlocks: (state, blocks) => {
        console.log(blocks);
        state.blocks = blocks;
    },
    createBlock: (state, { block, config }) => {
        block.children.push(config);
    },
    removeBlock: (state, { parent, index }) => {
        parent.children.splice(index, 1);
    },
    addBlock(state, { parent, child, index }) {
        parent.children.splice(index, 0, child);
    },
    modifyBlock: ({}, {target, prop, value}) => {
        Vue.set(target, 'size', value);
    },
};

/**
* Actions
*/

export const actions = {
    createBlock: ({ dispatch, commit, state }, { block, config }) => {
        config.children = [];
        config.content = '';
        config.settings = config.settings || {};
        config.identifier = Math.random().toString(36).substr(2, 9);
        commit('createBlock', { block, config });

        dispatch('save');
    },
    moveBlock: ({ dispatch, commit, state }, { fromBlock, toBlock, block, from, to}) => {
        commit('moveBlock', { fromBlock, toBlock, block, from, to });
        dispatch('save');
    },
    addBlock: ({ dispatch, commit }, data) => {
        commit('addBlock', data);
        dispatch('save');
    },
    removeBlock: ({ dispatch, state, commit }, { block, root }) => {
        let parent = root || state.blocks.root;
        let deletableIndex = parent.children.findIndex(child => child.identifier === block.identifier);

        if (deletableIndex !== -1) {
            commit('removeBlock', { parent, index: deletableIndex });
            return false;
        }

        parent.children.every(child => dispatch('removeBlock', { block, root: child }));

        dispatch('save');
    },
    createRow: ({ dispatch, state }, data = {}) => {
        dispatch('createBlock', {
            block: data.target || state.blocks.root,
            config: data.config || {
                type: 'block-row',
            },
        });

        dispatch('save');
    },
    createColumn: ({ dispatch, state }, data = {}) => {
        dispatch('createBlock', {
            block: data.target || state.blocks.root,
            config: data.config || {
                type: 'block-column',
                settings: {
                    size: 3,
                }
            },
        });

        dispatch('save');
    },
    async refresh({ dispatch, commit }) {
        await dispatch('fetch')
            .then((response) => {
                //console.log(response.data);
                commit('updateBlocks', response.data);
            })
            .catch((err) => {
                //console.log(err);
            });
    },
    async fetch ({ commit }) {
        return await this.$axios.get('/api/blocks');
    },
    save ({ state }) {
        this.$axios.post('/api/save', {
                blocks: state.blocks,
            })
            .then(response => {
                //console.log(response);
            })
            .catch((err) => {
                //console.log(err.message);
            });
    },
};
