import Vue from 'vue';

Vue.component('dnd-container', require('vue-smooth-dnd').Container);
Vue.component('dnd-draggable', require('vue-smooth-dnd').Draggable);

Vue.component('toolbar', require('~/components/tools/toolbar.vue').default);

Vue.component('block', require('~/components/blocks/block.vue').default);
Vue.component('blocks', require('~/components/blocks/blocks.vue').default);
Vue.component('block-row', require('~/components/blocks/grid/row.vue').default);
Vue.component('block-column', require('~/components/blocks/grid/column.vue').default);
Vue.component('block-text', require('~/components/blocks/text/text.vue').default);
