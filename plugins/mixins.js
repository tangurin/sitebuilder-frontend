import Vue from 'vue';

Vue.mixin({
    methods: {
        moveBlock(event) {
            if (event.removedIndex !== null) {
                this.$store.dispatch('blockz/removeBlock', {
                    block: event.payload,
                    root: this.block,
                });
            }
            if (event.addedIndex !== null) {
                this.$store.dispatch('blockz/addBlock', {
                    parent: this.block,
                    child: event.payload,
                    index: event.addedIndex
                });
            }
        },
    }
});
