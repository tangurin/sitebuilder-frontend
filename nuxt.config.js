const pkg = require('./package')


module.exports = {
    mode: 'universal',

    server: {
      port: 4000, // default: 3000
    },

    /*
    ** Headers of the page
    */
    head: {
        title: pkg.name,
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: pkg.description }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },

    /*
    ** Customize the progress-bar color
    */
    loading: { color: '#fff' },

    /*
    ** Global CSS
    */
    css: [
        '@/assets/scss/app.scss'
    ],

    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
            '~/plugins/mixins',
            '~/plugins/directives',
            '~/plugins/global-components',
    ],

    /*
    ** Nuxt.js modules
    */
    modules: [
        '@nuxtjs/axios',
    ],

    axios: {
       proxy: true,
       baseURL: 'http://127.0.0.1:4001/',
    },

    proxy: {
      '/api': 'http://127.0.0.1:4001/'
    },

    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {
        }
    },
    router: {
        routes: [
            {
                name: 'index',
                path: '/user',
                component: 'pages/index.vue'
            },
        ],
    },
}
